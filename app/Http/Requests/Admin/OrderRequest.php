<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	public function messages() {


		$message = [
			"has_many_order_reference.*.quantity.not_in"   => 'The quantity field is invalid.',
			"has_many_order_reference.*.quantity.required" => 'The quantity field required.',
			"branch_id.required"                           => 'The branch field required.',
		];

		foreach ($this->request->get('has_many_order_detail') as $key => $val) {
			$message[ 'has_many_order_detail.' . $key . '.quantity.not_in' ] = 'The quantity field is invalid.';
			$message[ 'has_many_order_detail.' . $key . '.quantity.required' ] = 'The quantity field required.';
		}


		if ($this->isMethod('put')) {

			$message['has_many_order_detail.required'] = 'The orders required.';
			$message['has_many_order_detail.min'] = 'The orders required.';
		}

		$message['has_many_order_reference.required'] = 'The orders required.';

		return $message;
	}


	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		$rules = [
			"has_many_order_reference.*.quantity" => 'required|not_in:0',
			"has_many_order_reference.*.quantity" => 'required|not_in:0',
			"branch_id"                           => "required",
			"has_many_order_reference"            => "required|array|min:1",
			"customer_id"            => "required",

		];


		if ($this->isMethod('put')) {
			$rules['invoice_number'] = 'required';

			$rules['has_many_order_detail'] = "required|array|min:1";

			if ($this->request->get('status_option_id') == 3) {

				unset($rules['has_many_order_detail']);
				unset($rules['invoice_number']);
			}

			if ($this->request->get('for_payment')) {
				unset($rules['invoice_number']);
				unset($rules['has_many_order_detail']);
			}


		}


		foreach ($this->request->get('has_many_order_detail') as $key => $val) {
			$rules[ 'has_many_order_detail.' . $key . '.quantity' ] = 'required|not_in:0';
		}

		return $rules;

	}
}
