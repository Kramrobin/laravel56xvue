<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ItemRequest extends FormRequest {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}


	public function messages() {
		return [
			'brand_id.required'           => 'The brand field required.',
			'category_header_id.required' => 'The category field required.',
			'code.required'               => 'The item code field required.',
			'category_detail_id.required' => 'The sub category field required.',
			'supplier_id.required'        => 'The supplier field required.',
			'package_id.required'         => 'The package field required.',
//			'phone_number.integer' => 'The phone number must be a number.',

		];
	}


	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {


		$rules = [
			//
			"name"               => 'required',
			"sku"                => 'required|unique:items',
			"srp"                => 'required',
			"code"               => 'required|unique:items',
			"brand_id"           => 'required',
			"category_header_id" => 'required',
			"category_detail_id" => 'required',
			"supplier_id"        => 'required',
			"package_id"         => 'required',
		];

		if ($this->isMethod('put')) {

			unset($rules['sku'], $rules['code']);
		}

		return $rules;
	}

}
