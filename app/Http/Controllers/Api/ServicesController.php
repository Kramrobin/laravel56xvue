<?php

namespace App\Http\Controllers\Api;

use App\Item;
use App\User;
use App\Admin;
use App\Brand;
use App\Promo;
use App\Branch;
use App\Package;
use App\Supplier;
use App\UserType;
use App\AdminRole;
use App\BranchType;
use App\Notification;
use App\StatusOption;
use App\CategoryDetail;
use App\CategoryHeader;
use App\BranchItemDetail;
use App\OrderPaymentType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class ServicesController extends Controller {

	/**
	 * @var CategoryHeader
	 */
	private $categoryHeader;
	/**
	 * @var CategoryDetail
	 */
	private $categoryDetail;
	/**
	 * @var Package
	 */
	private $package;
	/**
	 * @var Brand
	 */
	private $brand;
	/**
	 * @var Supplier
	 */
	private $supplier;
	/**
	 * @var UserType
	 */
	private $userType;
	/**
	 * @var Admin
	 */
	private $admin;
	/**
	 * @var Item
	 */
	private $item;
	/**
	 * @var Branch
	 */
	private $branch;
	/**
	 * @var BranchType
	 */
	private $branchType;
	/**
	 * @var AdminRole
	 */
	private $adminRole;
	/**
	 * @var BranchItemDetail
	 */
	private $branchItemDetail;
	/**
	 * @var StatusOption
	 */
	private $statusOption;
	/**
	 * @var User
	 */
	private $user;
	/**
	 * @var Promos
	 */
	private $promos;
	/**
	 * @var OrderPaymentType
	 */
	private $orderPaymentType;
	private $notification;

	/**
	 * @var Notification
	 */


	public function __construct(Notification $notification, Promo $promo, CategoryHeader $categoryHeader, CategoryDetail $categoryDetail, Package $package, Brand $brand, Supplier $supplier, UserType $userType, Admin $admin, Item $item, Branch $branch, BranchType $branchType, AdminRole $adminRole, BranchItemDetail $branchItemDetail, StatusOption $statusOption, User $user, OrderPaymentType $orderPaymentType) {


		$this->notification = $notification;
		$this->promo = $promo;
		$this->categoryHeader = $categoryHeader;
		$this->categoryDetail = $categoryDetail;
		$this->package = $package;
		$this->brand = $brand;
		$this->user = $user;
		$this->supplier = $supplier;
		$this->userType = $userType;
		$this->admin = $admin;
		$this->item = $item;
		$this->branch = $branch;
		$this->branchType = $branchType;
		$this->adminRole = $adminRole;
		$this->branchItemDetail = $branchItemDetail;
		$this->statusOption = $statusOption;
		$this->orderPaymentType = $orderPaymentType;
	}

	public function getPromos() {

		$response = $this->promo->get();

		return response()->json($response, 200);
	}

	public function getCategoryHeaders() {

		$response = $this->categoryHeader->get();

		return response()->json($response, 200);
	}

	public function getCategoryDetails() {

		$response = $this->categoryDetail->get();

		return response()->json($response, 200);
	}


	public function getBrands() {

		$response = $this->brand->get();

		return response()->json($response, 200);
	}


	public function getUsers() {

		$response = $this->user->get();

		return response()->json($response, 200);
	}


	public function getPackages() {

		$response = $this->package->get();

		return response()->json($response, 200);
	}

	public function getSuppliers() {

		$response = $this->supplier->get();

		return response()->json($response, 200);
	}

	public function getItems(Request $request) {
		//
		$data = $request->all();

		$branchName = 'All';
		$response = $this->item->with([
			'categoryHeader',
			'brand',
			'supplier',
			'hasManyBranchItemDetail',
			'hasOneBranchItemDetail' => function ($query) use ($data) {

				$query->with(['branch'])->orderBy('created_at', 'asc');

				if (isset($data['branch_id']) && $data['branch_id']) {

					$query->whereHas('branch', function ($query) use ($data) {
						$query->whereId($data['branch_id']);
					});

				}

			},
		])
			->withCount([
				'hasManyBranchItemDetail as has_many_branch_item_detail_count_quantity' => function ($query) use ($data) {

					if (isset($data['branch_id']) && $data['branch_id']) {

						$query->whereHas('branch', function ($query) use ($data) {
							$query->whereId($data['branch_id']);
						});


					}

					$query->select(DB::raw("SUM(quantity)"));
				},
			]);

		if (isset($data['keyword']) && $data['keyword']) {


			$response = $response->where('name', 'LIKE', '%' . $data['keyword'] . '%');
		}

		if (isset($data['category_header_id']) && $data['category_header_id']) {


			$response = $response->whereCategoryHeaderId($data['category_header_id']);
		}

		if (isset($data['brand_id']) && $data['brand_id']) {


			$response = $response->whereBrandId($data['brand_id']);
		}

		if (isset($data['supplier_id']) && $data['supplier_id']) {


			$response = $response->where('supplier_id', $data['supplier_id']);
		}

		if (isset($data['branch_id']) && $data['branch_id']) {

			$branch = $this->branch->whereId($data['branch_id'])->first();

			$branchName = $branch ? $branch->name : 'All';

			$response = $response->whereHas('hasManyBranchItemDetail', function ($query) use ($data) {
				$query->whereHas('branch', function ($query) use ($data) {
					$query->whereId($data['branch_id']);
				});
			});

		}

		if (isset($data['zero_quantity']) && json_decode($data['zero_quantity'])) {


			$response = $response->whereHas('hasManyBranchItemDetail', function ($query) use ($data) {
				$query->where('quantity', '!=', 0);
			});

		}


		$response = $response->paginate(isset($data['per_page']) ? $data['per_page'] : 10);
		$response->getCollection()->transform(function ($response) use ($branchName) {
			$colors = '';
			$response->branch_name = $branchName;

			foreach ($response->hasManyItemColor as $color) {
				$colors .= ', ' . $color->name;
			}
			$colors = substr($colors, 1);
			$response->colors = $colors ? $colors : '';

			return $response;
		});

		return response()->json($response, 200);
	}


	public function getItemByStocks(Request $request) {
		$data = $request->all();
		$response = $this->branchItemDetail->with(['item','supplier'])
			->where('quantity', '!=', 0);


		if (isset($data['branch_id']) && $data['branch_id']) {


			$response = $response->whereBranchId($data['branch_id']);
		}


		if (isset($data['supplier_id']) && $data['supplier_id']) {


//			$response = $response->whereBranchId($data['supplier_id']);
		}

		$response = $response->whereHas('item', function ($query) use ($data) {

			if (isset($data['keyword']) && $data['keyword']) {

				$query->where('name', 'LIKE', '%' . $data['keyword'] . '%');
			}

			if (isset($data['brand_id']) && $data['brand_id']) {
				$query->whereBrandId($data['brand_id']);

			}
			if (isset($data['category_header_id']) && $data['category_header_id']) {
				$query->whereCategoryHeaderId($data['category_header_id']);
			}
		});

		$response = $response->paginate(isset($data['per_page']) ? $data['per_page'] : 10);

		return response()->json($response, 200);
	}

	public function getUserTypes() {
		$response = $this->userType->get();

		return response()->json($response, 200);

	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function getSalesAgents(Request $request) {
		$data = $request->all();

		$response = $this->admin->whereIn('admin_role_id', [5, 6])->whereBranchId($data['branch_id'])->get();

		return response()->json($response, 200);

	}


	public function getBranches(Request $request) {
		$data = $request->all();

		$response = $this->branch;
		if (isset($data['id']) && $data['id']) {

			$response = $response->where('id','!=',$data['id']);
		}

		if (isset($data['branch_type_id']) && $data['branch_type_id']) {

			$response = $response->whereBranchTypeId($data['branch_type_id']);

		}

		$response = $response->get();

		return response()->json($response, 200);
	}

	public function getBranchTypes() {
		$response = $this->branchType->get();

		return response()->json($response, 200);
	}

	public function getAdminRoles() {
		$response = $this->adminRole->get();

		return response()->json($response, 200);
	}

	public function getStatusOptions(Request $request) {
		$data = $request->all();
		$response = $this->statusOption;
		if (isset($data['id']) && $data['id']) {


			$response = $response->whereIn('id', explode(',', $data['id']));

		}

		$response = $response->get();

		return response()->json($response, 200);
	}

	public function getPaymentTypes() {

		$response = $this->orderPaymentType->get();

		return response()->json($response, 200);
	}

	public function getNotifications(Request $request){

		$data = $request->all();

		$response = $this->notification->whereStatus('unread')->whereAnswerType($data['answer_type'])->whereAnswerId($data['answer_id'])->paginate(10);

		// return dd($response);
		$totalUnread = $this->notification->whereStatus('unread')->whereAnswerType($data['answer_type'])->whereAnswerId($data['answer_id'])->count();
		$response = collect(['total_unread' => $totalUnread])->merge($response);


		return response()->json($response, 200);
	}


}
