<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class BaseController extends Controller {
	//
	public function makeSlug($name, $model) {

		$m = App::make($model);

		$slug = str_slug($name);

		$count = $m->whereRaw("slug RLIKE '^{$slug}(-[0-9]+)?$'")->count();

		$s = $count ? "{$slug}-{$count}" : $slug;

		return $s;
	}

	public function addCookieToResponse(Request $request, array $data) {
	}


	public function generateOrderNumber($prefix, $type) {
		$data = App::make('App\OrderHeader')->whereOrderTypeId($type)->count();
		if ($data > 0) {
			$id = $data + 1;
		} else {
			$id = 1;
		}
		return $prefix . '-' . date("mdy") . '-' . $id;
	}

	public function logs($data){
		// return $data->message;
		
		$response = App::make('App\History');
		$response->action = $data['message'];

		$response->save();
	}


	public function getOrderType($action) {


		if ($action == 1) {
			$msg = "Franchise Order Created with Order Number: ";
		}
		if ($action == 2) {
			$msg = "InterOffice Order Created with Order Number: ";
		}
		if ($action == 3) {
			$msg = "Walkin Order Created with Order Number: ";
		}
		if ($action == 4) {
			$msg = "DR sample Created with Order Number: ";
		}
		if ($action == 5) {
			$msg = "Quotation Created with Order Number: ";
		}

		return $msg;
	}


	public function updateOrderType($action) {


		if ($action == 1) {
			$msg = "Franchise Order updated with Order Number: ";
		}
		if ($action == 2) {
			$msg = "InterOffice Order updated with Order Number: ";
		}
		if ($action == 3) {
			$msg = "Walkin Order updated with Order Number: ";
		}
		if ($action == 4) {
			$msg = "DR sample updated with Order Number: ";
		}
		if ($action == 5) {
			$msg = "Quotation updated with Order Number: ";
		}

		return $msg;
	}

}





//
//$lastId = InternalOrder::where('status','<',14)->count();
//if($lastId > 0){;
//	$autoIncrement = $lastId + 1;
//} else {
//	$autoIncrement = 1;
//}
//
//$orderNo = 'OR-'.date("mdy").'-'.$autoIncrement;