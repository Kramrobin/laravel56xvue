import Config from '../../configs/app.config';
import App from './app.component';
import LoginComponent from './components/auth/login.component';



//dashboard agent
import DashboardComponent from './components/dashboard/dashboard.component';



//accounts
import CrupAccountAdminComponent from './components/account/admin/crup-account-admin.component';
import ListAccountAdminComponent from './components/account/admin/list-account-admin.component';


//profile
// import ProfileComponent from './components/profile/profile.component';
//
//
// function requireRole(to, from, next) {
//
//     const authRoles = JSON.parse(auth.admin_role.roles);
//
//
//     const found = authRoles.some(el => el == to.meta.role);
//
//     if (found) {
//         next();
//
//     }else{
//         next(prefix + '/my-profile');
//     }
//
//
//
//
// }

export default [

    {
        name: 'app',
        path: Config.admin_prefix + '/',
        component: App,
        meta: {
            navigation: {
                parent: '',
                child: '',
            },
            title: 'Dashboard'
        }
    },
    {
        name: 'login',
        path: Config.admin_prefix + '/login',
        component: LoginComponent,
        meta: {
            navigation: {
                parent: '',
                child: '',
            },
            title: 'Login'
        }
    },
    {
        name: 'dashboard',
        path: Config.admin_prefix + '/dashboard',
        component: DashboardComponent,
        // beforeEnter: requireRole,
        meta: {
            navigation: {
                parent: 'dashboard',
                child: '',
            },
            // role: 'Dashboard',
            title: 'Dashboard'
        }
    },
    //accounts
    {
        name: 'admin-create',
        path: Config.admin_prefix + '/account/admins/create',
        component: CrupAccountAdminComponent,
        // beforeEnter : requireRole,
        meta: {
            navigation: {
                parent: 'account',
                child: 'admins',
            },
            role: "Accounts",
            title: 'Create Admin'
        }
    },
    {
        name: 'admin-update',
        path: Config.admin_prefix + '/account/admins/:id/edit',
        component: CrupAccountAdminComponent,
        // beforeEnter : requireRole,
        meta: {
            navigation: {
                parent: 'account',
                child: 'admins',
            },
            role: "Accounts",
            title: 'Update Admin'
        }

    },
    {
        name: 'admin-list',
        path: Config.admin_prefix + '/account/admins',
        component: ListAccountAdminComponent,
        // beforeEnter : requireRole,
        meta: {
            navigation: {
                parent: 'account',
                child: 'admins',
            },
            role: "Accounts",
            title: 'List of Admins'
        }
    },

];
