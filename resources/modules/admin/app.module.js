/**
 * First, we will load all of this project's Javascript utilities and other
 * dependencies. Then, we will be ready to develop a robust and powerful
 * application frontend using useful Laravel and JavaScript libraries.
 */

require('../../assets/bootstrap');
window.moment = require('moment');
window.Vue = require('vue');
window.auth = JSON.parse( localStorage.getItem('authAdmin'));
import VueRouter from 'vue-router';
import VueAxios from 'vue-axios';
import axios from 'axios';
import AppRouter from './app.router';
// import routes from './app.router';
import AdminSideBar from '../../cores/includes/admin-sidebar.component';
import AdminNavBar from '../../cores/includes/admin-navbar.component';
import AppMixin from './app.mixin';


Vue.use(VueRouter);
Vue.mixin(AppMixin);

Vue.component('admin-sidebar-component', AdminSideBar);
Vue.component('admin-navbar-component', AdminNavBar);




const router = new VueRouter(
    {
        mode: 'history',
        routes: AppRouter
    });


const app = new Vue(Vue.util.extend({

    router,
    mounted: function () {


        // this.initializeDatePicker();
    },
    methods: {

        initializeDatePicker: function () {

            //
            // $(document).ready(function () {
            //
            //
            //     var self = this;
            //     for (var i = 1; i <= $('.date-picker').length; i++) {
            //         $('#datepicker-' + i).datepicker({
            //             dateFormat: 'yy-mm-dd',
            //             onSelect: function (selectedDate, datePicker) {
            //                 self.date = selectedDate;
            //
            //             }
            //         });
            //     }
            //
            //
            // });
        }
    },
    watch: {
        $route(to, from) {
            // this.initializeDatePicker();
            console.log(to);
        }
    }


})).$mount('#app');
