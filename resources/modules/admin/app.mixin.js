import AlertService from "../../cores/services/alert/alert.service";

const alertService = new AlertService();
export default {
    data: function () {
        return {};
    },
    methods: {

        formatDate(date) {

            return moment(date).format('YYYY-MM-DD');
        },
        asQueryParams(data) {
            const searchParams = new URLSearchParams();
            const search = data;
            Object.keys(search).forEach(key => searchParams.append(key, search[key]));

            return "?" + searchParams.toString();
        },

        formatNumber(data) {

            return Math.round(parseFloat(data) * 100) / 100;
        },
        popErrors(errors) {
            $.notifyClose();
            Object.entries(errors).forEach(([key, value]) => {

                alertService.errorWithMessage(value[0]);

            });
        }

    }
};
