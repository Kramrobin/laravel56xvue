<?php



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', function () {

//	\Artisan::call('config:cache');
//	return redirect('/' . \Config::get('urlsegment.admin_prefix') . '/dashboard');
});

Auth::routes();




$router->group(['prefix' => \Config::get('urlsegment.admin_prefix'), 'namespace' => 'Admin\Auth'], function ($router) {

	$router->get('login', 'LoginController@showLoginForm')->name(\Config::get('urlsegment.admin_prefix') . '.login');
	$router->post('login', 'LoginController@login');
	$router->get('logout', 'LoginController@logout');

//	$router->post('admin-password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
//	$router->get('admin-password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
//	$router->post('admin-password/reset', 'ResetPasswordController@reset');
//	$router->get('admin-password/reset/{token}', 'ResetPasswordController@showResetForm')->name('admin.password.reset');
});


