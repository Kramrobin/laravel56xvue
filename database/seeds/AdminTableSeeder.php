<?php

use Illuminate\Database\Seeder;
use App\Admin;


class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         // Let's clear the users table first
         Admin::truncate();

         $faker = \Faker\Factory::create();
 
         // Let's make sure everyone has the same password and 
         // let's hash it before the loop, or else our seeder 
         // will be too slow.
         $password = Hash::make('password');
 
         Admin::create([
             'name' => 'Administrator',
             'email' => 'admin@test.com',
             'admin_role_id' => '1',
             'branch_id' => '1',
             'max_discount' => '100',
             'password' => $password,
         ]);
 
         // And now let's generate a few dozen users for our app:
         for ($i = 0; $i < 10; $i++) {
            // $faker = \Faker\Factory::create();
            Admin::create([
                 'name' => $faker->name,
                 'email' => $faker->email,
                 'password' => $password,
             ]);
         }
    }
}
